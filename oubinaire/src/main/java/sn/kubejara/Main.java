package sn.kubejara;

import java.util.Scanner;

public class Main {
    public static char ZERO ='0';
    public static char UN ='1';

    public static void main(String[] args) {
        Scanner in  = new Scanner(System.in);
        String n1 = in.next();
        String n2 = in.next();
        StringBuilder result = new StringBuilder();
        for(int i=0; i<n1.length(); i++){
            if(n1.charAt(i)==ZERO && n2.charAt(i)==ZERO){
                result.append(ZERO);
            }else {
                result.append(UN);
            }
        }
        System.out.println(result);

    }
}