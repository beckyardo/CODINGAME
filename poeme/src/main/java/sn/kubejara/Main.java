package sn.kubejara;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String linesSeparator = in.nextLine();
        String stanzasSeparator = in.nextLine();
        String alienPoem = in.nextLine();

        String[] arrayOfStanzas =  alienPoem.split(String.format("\\%s",stanzasSeparator));

        for(int i=0 ; i <arrayOfStanzas.length; i++){
            String[] stanzaLines = arrayOfStanzas[i].split(String.format("\\%s",linesSeparator));
            for (int j=0; j<stanzaLines.length; j++){
                System.out.println(stanzaLines[j]);
            }
            System.out.println();
        }


    }
}