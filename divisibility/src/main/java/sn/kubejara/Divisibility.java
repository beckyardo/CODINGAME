package sn.kubejara;

public class Divisibility {
    public static  final String SPRING ="Spring";
    public static  final String BOOT ="Boot";
    public static  final int VALUE_SEVEN = 7;
    public static  final int VALUE_ELEVEN = 11;

    public static String divisibility(int value){
        StringBuilder stringBuilder = new StringBuilder();

        if(value % VALUE_SEVEN ==0 && value % VALUE_ELEVEN ==0){
            stringBuilder.append(SPRING).append(BOOT);
        } else if (value % VALUE_ELEVEN ==0) {
            stringBuilder.append(BOOT);
        } else if (value % VALUE_SEVEN ==0) {
            stringBuilder.append(SPRING);
        }
        return stringBuilder.toString();
    }
}
